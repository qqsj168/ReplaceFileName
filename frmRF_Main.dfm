object frmRFMain: TfrmRFMain
  Left = 395
  Top = 100
  Width = 467
  Height = 350
  Caption = #26367#25442#25991#20214#21517
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object mmoInfo: TMemo
    Left = 0
    Top = 193
    Width = 459
    Height = 126
    Align = alClient
    BevelOuter = bvNone
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 459
    Height = 193
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      459
      193)
    object lbl5: TLabel
      Left = 28
      Top = 14
      Width = 48
      Height = 12
      Caption = #30446#24405#21517#65306
    end
    object lbl1: TLabel
      Left = 28
      Top = 40
      Width = 48
      Height = 12
      Caption = #26597'  '#25214#65306
    end
    object lbl2: TLabel
      Left = 28
      Top = 67
      Width = 48
      Height = 12
      Caption = #26367#25442#20026#65306
    end
    object lbl3: TLabel
      Left = 28
      Top = 91
      Width = 48
      Height = 12
      Caption = #25193#23637#21517#65306
    end
    object edtOld: TEdit
      Left = 83
      Top = 36
      Width = 350
      Height = 20
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
    end
    object btnPath: TcxButtonEdit
      Left = 83
      Top = 10
      Anchors = [akLeft, akTop, akRight]
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = btnPathPropertiesButtonClick
      Style.BorderStyle = ebs3D
      TabOrder = 1
      Width = 350
    end
    object edtNew: TEdit
      Left = 83
      Top = 63
      Width = 350
      Height = 20
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 2
    end
    object btnReplace: TButton
      Left = 95
      Top = 159
      Width = 75
      Height = 25
      Caption = #26367#25442
      TabOrder = 3
      OnClick = btnReplaceClick
    end
    object btnCancel: TButton
      Left = 268
      Top = 159
      Width = 75
      Height = 25
      Caption = #21462#28040
      Enabled = False
      TabOrder = 4
      OnClick = btnCancelClick
    end
    object edtFileType: TEdit
      Left = 83
      Top = 87
      Width = 350
      Height = 20
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 5
      Text = '*.*'
    end
    object chkSerachDir: TCheckBox
      Left = 83
      Top = 112
      Width = 75
      Height = 17
      Caption = #23376#30446#24405
      TabOrder = 6
    end
    object chkReplaceDir: TCheckBox
      Left = 192
      Top = 112
      Width = 94
      Height = 17
      Caption = #30446#24405#21517#26367#25442
      TabOrder = 7
      OnClick = chkReplaceDirClick
    end
    object chkOnlyReplaceDir: TCheckBox
      Left = 323
      Top = 112
      Width = 97
      Height = 17
      Caption = #20165#26367#25442#30446#24405
      TabOrder = 8
    end
    object chkCaseSensitive: TCheckBox
      Left = 83
      Top = 136
      Width = 92
      Height = 17
      Caption = #21306#20998#22823#23567#20889
      TabOrder = 9
    end
    object chkWholeWord: TCheckBox
      Left = 192
      Top = 136
      Width = 75
      Height = 17
      Caption = #20840#23383#21305#37197
      TabOrder = 10
    end
    object chkOnlyFileExt: TCheckBox
      Left = 323
      Top = 136
      Width = 104
      Height = 17
      Caption = #20165#26367#25193#23637#21517
      TabOrder = 11
    end
  end
  object frmstrg1: TFormStorage
    IniFileName = './RFMain.ini'
    Options = []
    StoredProps.Strings = (
      'btnPath.Text'
      'edtFileType.Text'
      'edtNew.Text'
      'edtOld.Text'
      'chkReplaceDir.Checked'
      'chkSerachDir.Checked'
      'chkCaseSensitive.Checked'
      'chkOnlyFileExt.Checked'
      'chkOnlyReplaceDir.Checked'
      'chkWholeWord.Checked')
    StoredValues = <>
    Left = 65528
    Top = 65533
  end
end
