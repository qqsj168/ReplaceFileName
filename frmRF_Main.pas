unit frmRF_Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxButtonEdit, StdCtrls, cmp_GlobalFuns, Placemnt, ExtCtrls;

type
  TRFReplaceFile = record
    //类型
    Attr: Integer;
    //文件名(括扩展名)
    FileName: string;
    //文件名(不包括扩展名)
    Name: string;
    //扩展名
    FileExt: string;
    //文件所在的路径
    path: string;
  end;

  TRFReplaceFiles = array of TRFReplaceFile;

  TfrmRFMain = class(TForm)
    lbl5: TLabel;
    btnPath: TcxButtonEdit;
    lbl1: TLabel;
    edtOld: TEdit;
    lbl2: TLabel;
    edtNew: TEdit;
    btnReplace: TButton;
    btnCancel: TButton;
    lbl3: TLabel;
    edtFileType: TEdit;
    frmstrg1: TFormStorage;
    chkSerachDir: TCheckBox;
    mmoInfo: TMemo;
    pnl1: TPanel;
    chkReplaceDir: TCheckBox;
    chkOnlyReplaceDir: TCheckBox;
    chkCaseSensitive: TCheckBox;
    chkWholeWord: TCheckBox;
    chkOnlyFileExt: TCheckBox;
    procedure btnReplaceClick(Sender: TObject);
    procedure btnPathPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure btnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure chkReplaceDirClick(Sender: TObject);
  private
    { Private declarations }
    FIsCancel: boolean;
    FReplaceFiles: TRFReplaceFiles;

    function isReplace(findStr, str: String): Boolean;
    procedure findFileName(sr: TSearchRec; strDir: string);


    function replaceFileName(rfFile: TRFReplaceFile): boolean;
    procedure replaceFileNames();
  public
    { Public declarations }
  end;

var
  frmRFMain: TfrmRFMain;

implementation

{$R *.dfm}

{ TfrmRFMain }

procedure TfrmRFMain.findFileName(sr: TSearchRec; strDir: string);
var
  index: integer;
  b: Boolean;
  fileName, ext, str: string;
begin
  if (chkOnlyReplaceDir.Checked) and (sr.Attr <> faDirectory) then
  begin
    Exit;
  end;
  fileName := sr.Name;
  ext := ExtractFileExt(fileName);
  fileName := copy(fileName, 1, length(fileName) - length(ext));
  str := fileName;
  if chkOnlyFileExt.Checked then
  begin
    str := ext;
  end;  
  b := isReplace(edtOld.Text, str);
  if b then
  begin
    index := length(FReplaceFiles);
    SetLength(FReplaceFiles, index + 1);
    FReplaceFiles[index].FileName := sr.Name;
    FReplaceFiles[index].Name := fileName;
    FReplaceFiles[index].FileExt := ext;
    FReplaceFiles[index].Attr := sr.Attr;
    FReplaceFiles[index].path := strDir;
  end;
end;

procedure TfrmRFMain.btnReplaceClick(Sender: TObject);
begin
  if Application.MessageBox('替换后不可恢复，确定要替换吗？', '询问框',
    mb_YesNo+mb_IconQuestion+MB_DEFBUTTON2) = IDNO then
  begin
    Exit;
  end;
  try
    if Trim(btnPath.Text) = '' then
    begin
      ShowMessage('文件夹不能为空！');
      exit;
    end;
    if edtOld.Text = '' then
    begin
      ShowMessage('原字符串不能为空！');
      exit;
    end;
    mmoInfo.Clear;
    FIsCancel := false;
    SetLength(FReplaceFiles, 0);
    btnCancel.Enabled := true;
    TGlobalFuns.FindFileAll(btnPath.Text, FIsCancel, findFileName, chkSerachDir.Checked,
      edtFileType.Text, chkReplaceDir.Checked);

    replaceFileNames;
    SetLength(FReplaceFiles, 0);
  finally
    btnCancel.Enabled := false;
  end;

end;

procedure TfrmRFMain.btnPathPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  strPath: string;
begin
  strPath := TGlobalFuns.BrowseForFolder('请选择目录', trim(btnPath.Text));
  if trim(strPath) = '' then exit;
  strPath := strPath + '\';
  btnPath.Text := strPath;
end;

procedure TfrmRFMain.btnCancelClick(Sender: TObject);
begin
  FIsCancel := true;
end;

procedure TfrmRFMain.replaceFileNames;
var
  i, findCount, replaceCount: integer;
begin
  FIsCancel := false;
  findCount := Length(FReplaceFiles);
  replaceCount := 0;
  for i := high(FReplaceFiles)  downto low(FReplaceFiles) do
  begin
    Application.ProcessMessages;
    if FIsCancel then
    begin
      exit;
    end;  
    if (replaceFileName(FReplaceFiles[i]))then
    begin
      Inc(replaceCount);
    end;
  end;
  mmoInfo.Lines.Add(Format('替换结果：%d/%d(个/共)', [replaceCount, findCount]));
end;

function TfrmRFMain.replaceFileName(rfFile: TRFReplaceFile): Boolean;
var
  b: Boolean;
  strNew, oldFile, newFile, path, strInfo, strNewExt, str: string;
  ReplaceFlags: TReplaceFlags;
begin
  result := false;
  str := rfFile.Name;
  if chkOnlyFileExt.Checked then
  begin
    str := rfFile.FileExt;
  end;  
  b := isReplace(edtOld.Text, str);
  if b then
  begin
    ReplaceFlags := [rfReplaceAll];
    if not chkCaseSensitive.Checked then
    begin
      ReplaceFlags := ReplaceFlags + [rfIgnoreCase];
    end;
    strNew := rfFile.Name;
    strNewExt := rfFile.FileExt;
    if chkOnlyFileExt.Checked then
    begin
      strNewExt := StringReplace(strNewExt, edtOld.Text, edtNew.Text, ReplaceFlags);
    end else
    begin
      strNew := StringReplace(strNew, edtOld.Text, edtNew.Text, ReplaceFlags);
    end;
    path := rfFile.path;
    if path[Length(path)] <> '\' then
    begin
      path := path + '\';
    end;
    oldFile := path + rfFile.FileName;
    if (Length(strNewExt) > 0) and (strNewExt[1] <> '.') then
    begin
      strNewExt := '.' + strNewExt;
    end;   
    newFile := path + strNew + strNewExt;
    strInfo := '文件名：' + path + rfFile.Name + ' => ' + strNew + ' -- ';
    result := RenameFile(oldFile, newFile);
    if not result then
    begin
      strInfo := strInfo + '失败！';
      mmoInfo.Lines.Add(strInfo);
    end;
  end;
end;

procedure TfrmRFMain.FormShow(Sender: TObject);
begin
  chkOnlyReplaceDir.Enabled := chkReplaceDir.Checked;
end;

procedure TfrmRFMain.chkReplaceDirClick(Sender: TObject);
begin
  chkOnlyReplaceDir.Enabled := chkReplaceDir.Checked;
end;

function TfrmRFMain.isReplace(findStr, str: string): Boolean;
begin
  //result := False;
  //不区分大小写
  if not chkCaseSensitive.Checked then
  begin
    str := UpperCase(str);
    findStr := UpperCase(findStr);
  end;
  if chkWholeWord.Checked then
  begin
    Result := str = findStr;
    exit;
  end;
  Result := Pos(findStr, str) > 0;
end;

end.
